import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert } from 'typeorm';
import { IsEmail } from 'class-validator';
import GravatarModule from 'gravatar';
import * as crypto from 'crypto';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  avatar: string;

  @BeforeInsert()
  hasAvatar() {
    if (!this.avatar) {
      this.avatar = GravatarModule.url(this.email);
    }
  }

  @Column()
  password: string;

  @BeforeInsert()
  hasPassword() {
    const md5 = crypto.createHash('md5');
    md5.update(this.password);
    this.password = md5.digest('hex');
  }
}
