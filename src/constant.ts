const DB_CONNECTION_TOKEN = 'DbConnectionToken';
const USER_REPOSITORY_TOKEN = 'UserRepositoryToken';

export { DB_CONNECTION_TOKEN, USER_REPOSITORY_TOKEN };
