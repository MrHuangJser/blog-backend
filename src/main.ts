import { NestApplicationOptions } from '@nestjs/common/interfaces/nest-application-options.interface';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const appOptions: NestApplicationOptions = { cors: true };
  const app = await NestFactory.create(AppModule, appOptions);
  app.setGlobalPrefix('/api');
  const docOptions = new DocumentBuilder()
    .setTitle('blog backend')
    .setDescription('blog backend api')
    .setBasePath('/api')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, docOptions);
  SwaggerModule.setup('/docs', app, document);
  await app.listen(3001, () => console.log(`app is listen on 3001`));
}
bootstrap();
