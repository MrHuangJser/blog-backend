import {
  Middleware,
  NestMiddleware,
  MiddlewareFunction,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, NextFunction, Response } from 'express';

@Middleware()
export class AuthMiddleware implements NestMiddleware {
  constructor() {}

  resolve(): MiddlewareFunction {
    return async (req: Request, res: Response, next) => {
      if (
        req.headers.authorization &&
        (req.headers.authorization as string).split(' ')[0] === 'Token'
      ) {
        // const token = (req.headers.authorization as string).split(' ')[1];
      } else {
        throw new HttpException('not authorized', HttpStatus.UNAUTHORIZED);
      }
    };
  }
}
